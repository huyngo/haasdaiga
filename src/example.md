# Examples

Here are some collections of sample:

## Translations

- [XKCD][xkcd] (webcomic)
- Schneewittchen (German fairy tale)
- Le Petit Prince (French novella)
- The Legend of Saint Gióng (Vietnamese legend)

I would like to translate *A Game of Throne* prologue and some chapters in *The
Lord of the Rings* as well, but since they're not yet in public domain and they
are not published under a free license (e.g. Creative Commons), I would avoid
them for legal reasons, even though such use might be considered as fair use.

[xkcd]: https://git.disroot.org/huyngo/haasdaiga-xkcd
