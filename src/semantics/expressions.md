# Fixed Expressions 

| Purpose   | English | Hàäsdáïga literal translation[^1] |
|-----------|---------|---------------------|
| Greeting  | Hello   |                     |
| Apologize | Sorry   | I hope that you me.acc forgive.sub.fut |
| Express sympathy | Sorry |                     |

[^1]: For the sake of simplicity and generality, only noun cases (if not
  nominative) and verb mood (if not indicative) is used.
