# Proper name and translation

## Given name gender

As a tradition, ancient Gaidaanan people gave names to newborns based with
gender based on the month of the year, as did people during the story
timeline.  This is obvious with Hàäsdáïga month names, but it is not that
transparent to use in our world, which use 12 months in both solar and lunar
calendar (I am not aware of any other calendar system).  For that reason, I
propose that we instead decide based on lunar months:

- Wood if the person is born from month 1-3
- Fire if the person is born from month 4-6
- Metal if the person is born from month 7-9
- Water if the person is born from month 10-12

This corresponds to the four seasons.  Optionally, people born in month 6 or 7
can be assigned gender Earth.

## Family name gender

Family names usually have meaning.  If it is a noun (e.g. Poisson, Rice, Wood,
Pierre, Lagrange, ...) or adjective (Legrand, Leblanc, Weiss), they carry the
same gender as that corresponding noun or verb, even if it is not translated.
If it is a profession, which does not have a default gender, it would
theoretically be the gender of the first person carrying that name.  That is
however not feasible, so instead it is associated with the thing that
profession works on.  For example, Smith → metal, Gärtner → wood.  For names of
unknown origin (Johnson, James, Nguyễn), just choose the first vowel in the
name for gender.

All family names in Hàäsdáïga are nouns, though most people don't use it.
