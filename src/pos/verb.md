# Verb

## Tense

There are 3 tenses: past, present, and future.  They are used according to
time.

### Reported speech

Unlike in English and some other languages, tense is not propagated in reported speech.
Instead, the tense is relative to the time spoken.
For example, if John said to Mary "I will meet you at noon" and now
is after that, you would say:

>
> John-nom.sg Mary-dat.sg say-ind.pst.prf `<sub>` he-nom.sg her-acc.sg meet-ind.pst.prf
> noon-dat.fi.sg  
> *John told Mary he would meet her at noon.*

If the future is indefinite, it will stays in the future, whether the event has
happened or not.

> 
> he-nom.fi.sg believe-ind.pst.cont `<sub>` she-nom.ea.sg back-come-ind.fut.prf,
> that-acc.sg she-nom.ea.sg do.ind.pst.prf  
> *He believed she would come back, and she did.*

### Narration

Narration is always present tense, even when the story obviously happened in the past.

## Mood

- indicative: the action actually occurs
- interrogative: the action is in the question
- imperative: the action is a command
- subjunctive: the action does not happens, and usually used in conditional or wish
- ritual: the action is the intend of the spellcaster

Ritual mood is also used for cursing and profanity.  In such case, the gender
affix is removed so the speaker does not accidentally cast the actual curse.
Because cursing is quite literal for Hàäsdáïga speakers, it is never used in
the same manner as in the modern society.

## Aspect

- habitual: the action happens repeatedly in the period of time
- continuous: the action or the state persists over the duration
- perfective: the action happens once and finished
