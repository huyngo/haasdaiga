# Lexicon

Below are the list of words in the lexicon.  They're translated form Hàäsdáïga
to English.

Abbreviations:

| abbreviation | meaning     |
|--------------|-------------|
| adv.         | adverb      |
| n.           | noun        |
| v.           | verb        |
| pp.          | postposition|
| pron.        | pronoun     |
## A 

**aäratse**
   1. (n.) ash

## B 


**bansy**
   1. (pron.) someone
   2. (pron., interrogative), who

**bissy**
   1. (n.) man

**bochzo**
   1. (n.) repair

## C 


**càhánza**
   1. (v.) give

**càïzo**
   1. (v.) can

**cèë**
   1. (interjection) no
   2. (adv.) not

**chaäswúze**
   1. (v.) to award, to give a prize

**chát**
   1. (pp.) with

**chech**
   1. (conj.) but

**chon**
   1. (adv.) never

**chónso**
   1. (n.) machine

**chopmensu**
   1. (n.) thought

**chopzu**
   1. (v.) think

**chỳc**
   1. (pp.) near

**cicso**
   1. (n.) angle

**cong**
   1. (pp.) opposite of

**connyzo**
   1. (v.) know (personal)

**cotsy**
   1. (pron.) somehow
   2. (pron., interrogative) how

## D 


**dácza**
   1. (v.) search for
   2. (v.) find

**daä**
   1. (interjection) yes
   2. (adv.) an emphasizing particle

**daüza**
   1. (v.) make, create

**daïnèza**
   1** (v.) help

**dàm**
   1. (pp.) beyond

**daänrása**
   1. (n) plant
   2. (n.) tree

**dún**
   1. (pp.) up

**dúnjgaso**
   1. (n.) prize

**dy**
   1. (particle) that (subordinate particle, `<sub>`)

## F 


**fá**
   1. (pp.) across, through

**fènvynsỳ**
   1. (pron., dual, exclusive) we

**fènsy**
   1. (pron.) I

**fènvynsỳ**
   1. (pron., dual, inclusive) we

## G 


**gaiza**
   1. (v.) start, begin

**ghósmense**
   1. (n.) drawing, picture

**ghósze**
   1. (v.) draw

**gonsi**
   1. (n.) rock, stone

**gòngze**
   1. (v.) look

## H 


**hipsy**
   1. (pron.) somewhen
   2. (pron., interrogative) when

## J 


**jaächdynsỳ**
   1. (pron., several, inclusive) we

**jaächsỳ**
   1. (pron., several, exclusive) we

**jannase**
   1. (n.) name

**jinsi**
   1. (n.) day (the time for the planet to complete a rotation)
   2. (n.) date

**jím**
   1. (pp.) under, below

**jos**
   1. (pp.) inside

## L 


**leë'chanzo**
   1. (v.) calculate

**lìmsi**
   1. (n.) human
   2. (n.) person

**lìmgáchsi**
   1. (n.) giant

**lìmzìssi**
   1. (n.) dwarf

**lòn**
   1. (pp.) down

**long**
   1. (pp.) between

**lúccynsỳ**
   1. (pron., dual) you

**lúcsy**
   1. (pron., singular) thou

## M 


**meëráze**
   1. (v.) love (personal)

**mèng**
   1. (pp.) in front of

**mochso**
   1. (n.) net

**músnech**
   1. (pp.) after

**mỳfgháä**
   1. (pp.) before

**mỳsu**
   1. (n.) glass

## N 


**naändèsi**
   1. (n.) world

**néëcsỳ**
   1. (pron., several) you

**ngèëse**
   1. (n.) day (as opposed to night)

**ngeënsý**
   1. (pron., plural) you

**ngỳch**
   1. (pp.) above

**nìchzi**
   1. (v.) buy

**niï**
   1. (adv.) not yet

**njésy**
   1. (n.) woman

**njizi**
   1. (v.) do

**njongù**
   1. (pp.) during

**nùchsa**
   1. (n.) kid, child

**nùchzìssa**
   1. (n.) baby

**nunjsu**
   1. (n.) night

## P 


**pam**
   1. (pp.) over

**panatzo**
   1. (v.) to gift, to give something as a present
   2. (v.) to dedicate (to the public)
   3. (v.) to dedicate (to someone, in an art work)

**púüriza**
   1. to be born

## R 


**raäse**
   1. (n.) fire

**rèëchdynsý**
   1. (pron.) we (plural, inclusive)

**rèëchsý**
   1. (pron.) we (plural, inclusive)

**réïn**
   1. (adv.) always, forever

**ringyso**
   1. (n.) vehicle

**rung**
   1. (pp.) outside

**rypnersy**
   1. (n.) teacher

**rypzi**
   1. (v.) teach

## S 


**saändasi**
   1. (n.) city

**sànj**
   1. (pp.) without

**sechse**
   1. (n.) gift

**sés**
   1. (pp.) until

**siïvỳlmensa**
   1. (n.) life

**siïvỳlza**
   1. (v.) live

**siïpzu**
   1. (v.) send

**synza**
   1. (v.) be new, be young (age)

**súngsa**
   1. (n.) book

## T 


**tèf**
   1. (pp.) behind

**tèndynsỳ**
   1. (pron., dual) they

**tènsy**
   1. (pron.) ze (singular third person pronoun)

**téërense**
   1. (n.) star

## V 


**valusa**
   1. (n.) number

**vamzi**
   1. (v.) stay

**váp**
   1. (pp.) to

**vàtsy**
   1. (pron.) somewhere
   2. (pron., interrogative) where

**vifzi**
   1. (v.) stand

**voözi**
   1. (v.) be located at (for immobile objects like cities, mountains)

**voc**
   1. (adv.) already

**voïczu**
   1. (v.) become

**vungze**
   1. (v.) happen

## W 


**wítzi**
   1. (v.) be (copula)

**wócsi**
   1. (n.) shop, store, market

**wuüpsi**
   1. (n.) dog

**wuüpzìssi**
   1. (n.) puppy

## Z 


**zápmỳso**
   1. (n.) mirror

**zápzu**
   1. (v.) reflect

**zaätza**
   1. (v.) to say
   2. (v.) to speak
