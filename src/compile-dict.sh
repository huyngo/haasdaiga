cat lexicon.dict.txt | sed "/^=/d" | sed -e '/./b' -e :n -e 'N;s/\n$//;tn' |\
	dictfmt -j --utf8 --utf8 \
	-s "Hàäsdáïga-English dictionary" \
	-u https://git.disroot.org/huyngo/conlang hdg-eng
