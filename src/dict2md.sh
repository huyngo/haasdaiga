# Convert lexicon.dict.txt to better markdown

cp lexicon.dict.txt lexicon.temp
sed "s/=/##/g" lexicon.temp > lexicon.tmp
mv lexicon.tmp lexicon.temp
sed "s/#*$//g" lexicon.temp > lexicon.tmp
mv lexicon.tmp lexicon.temp
sed "s/:/**/g" lexicon.temp > lexicon.tmp
mv lexicon.tmp lexicon.temp
{ cat lexicon.template.md ; cat lexicon.temp; } > lexicon.md;
rm lexicon.temp
