# Phonology

<!-- toc -->

## Consonants

Main consonants are listed in the table below:

|               | labial | alveolar | palatal | uvular    | glottal |
|---------------|--------|----------|---------|-----------|---------|
| **nasal**     | m      | n        | ɲ (*nj*)  | ŋ (*n*, *ng*) |         |
| **plosive**   | p b    | t d      |         | k (*c*) ɡ (*g*)   | ʔ       |
| **fricative** | f v    | s z      |         | x (*ch*) ɣ (*gh*) | h   |
| **sonorant**  | w      | r l      | j       |           |         |

Notes:

- *n* is pronounced as /ŋ/ only if it is followed by another consonant
- The glottal stop /ʔ/ is not written. It's implied when a syllable does not
  have other consonants (similar to Vietnamese).
- *nj* is pronounced as /ɲ/ even when the two letters are from different words.
  The sames go for other digraphs (ng, ch, gh).  There is therefore no
  ambiguous way of pronouncing a word given a romanization.
- Some dialects may pronounce *s*, *z* as 

## Vowels

|       | front | centre | back |
|-------|-------|--------|------|
| open  | i     |        | u    |
| mid   | e     | ə (*y*)| o    |
| close | a     |        |      |

Note: the schwa sound /ə/ is not written in the native script.  However, for
ease of writing on computer, it is romanized as y.  Both kýn and ḱn are
acceptable as the romanization for the same syllable.  This document is in
favor of using explicit form instead of implicit form.

### Diphthongs and long vowels

Long vowels are written as duplicated vowels, with the second character having
a diaresis (for example *aä* is pronounced /aː/). For vowels whose
romanizations contain two characters, the later vowel is duplicated (for
example, long version of *aë* is *aëë*)

Diphthongs are written similarly. They're listed below:

- aï /aɪ/
- aü /aʊ/
- eï /eɪ/
- oï /oɪ/
- uï /uɪ/

## Tones

There are three tones in Hàäsdáïga:

- level tone (a) /˧˧/
- rising tone (á) /˧˥/
- falling tone (à) /˥˩/

## Phonotactics

Syllables in Hàäsdáïga have a simple structure: **CVT(C)**

The final consonants can't be sonorants, /h/, /ʔ/, and voiced vowels if they
have voiceless equivalents.  Other than that, there isn't any constraints.

There are thus:

\\( 22  × (6 × 2 + 6 )  × 3  × 11  = 18876  \\) (possible syllables)

Note: These numbers mean:

- 22 consonants
- 6 vowels and their long version
- 6 diphthongs
- 3 tones
- 10 final consonants
