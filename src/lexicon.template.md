# Lexicon

Below are the list of words in the lexicon.  They're translated form Hàäsdáïga
to English.

Abbreviations:

| abbreviation | meaning     |
|--------------|-------------|
| adv.         | adverb      |
| n.           | noun        |
| v.           | verb        |
| pp.          | postposition|
| pron.        | pronoun     |
