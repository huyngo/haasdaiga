## Verbal

Verbal conjugation are decided by mood, tense, and aspect.
Infinitive verbs all have the ending -zy, which is changed by conjugation.
They are respectively indicated by a vowel, a tone, and a consonant.

### Mood

There are five moods:

- **ind** indicative: a
- **rit** ritual: e
- **int** interrogative: i
- **imp** imperative: o
- **sub** subjunctive: u

### Tense

- **pst** past: falling
- **prs** present: level
- **fut** future: rising

### Aspect

- **hab** habitual: j
- **cnt** continuous: r
- **prf** perfective: unmarked

### Gender

In the ritual mood, another vowel representing verb's gender is added after the
aspect consonant.  The vowel is the same as for nominal gender.
Note that it is not uncommon to use ritual mood without gender, such as when
insulting.

The dictionary form shows this gender even though it is not in ritual mood.
