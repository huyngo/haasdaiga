# Sentence order

In conversation, the sentence order is relatively loose: SVO, OSV, VSO, VOS,
SOV are all used. Particularly, they're preferred to be used like described
below:

- SVO: when the subject is the topic of the sentence
- OSV: when the object is the topic of the sentence; it's also used as
    equivalent for passive voice, which Hàäsdáïga does not have
- VSO: when the verb is the topic, or when the question is about the object
- VOS: when it's a question and it's about the subject
- SOV: when the object is a pronoun, especially a reflexive pronoun.

Examples:

> he-nom.ea.sg have-ind.prs.cnt many money-acc.mtl.pl  
> *He has a lot of money.*
>
> die-RESULT-acc.wa.sg he-nom.fi.sg not fear-ind.prs.cnt  
> *Death, he does not fear.*
>
> curse-int.pst.prf you-nom.wo.sg neck-wearing-acc.mtl.sg not?  
> *Did you curse that necklace?*
>
> choose-int.fut.prf you-nom.wo.sg path-acc.ea.sg  
> *Which path will you choose?*
>
> pay-int.fut.prf meal-money-acc.mtl.pl who.nom.sg  
> *Who will pay for the meal?*
>
> I-nom.fi.sg you-acc.wa.sg love-ind.prs.cnt  
> *I love you.*

While casting magic, the sentence order is always SVO.  Here is an example of
air shield spell, which is a very simple one:

> air-nom.wa.pl flow-rit.prs.cnt.wa and protect-rit.prs.cnt.ea I-acc.wa.sg  
> *The air shall flow around me and protect me.*
