# Negation

To negate a sentence, simply put the negative adverb in front of the verb.
For each sense of negativity, there is a separate word:

- not: cèë
- not yet: niï
- never: chon

Following are the opposite of them: 

- yes: daä
- already: voc
- always: réïn
